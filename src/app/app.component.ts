import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from './services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-sample';
  isLogged = false;

  constructor(private router: Router, private tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.tokenStorageService.tokenSubject.subscribe(token => {
      this.isLogged = !!token;
    });
  }

  logout(): void{
    this.tokenStorageService.clearToken();
    this.router.navigate(['/login'])
  }
}
