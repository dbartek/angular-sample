import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  public tokenSubject = new Subject()

  private token: string | null = null

  constructor() { }

  getToken(): string | null {
    return this.token;
  }

  saveToken(token: string): void {
    this.token = token;
    this.tokenSubject.next(token);
  }

  clearToken(): void {
    this.token = null;
    this.tokenSubject.next(null);
  }
}
