import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

export interface ILoginResult {
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() { }

  login(login: string, password: string): Observable<ILoginResult> {
    if(login === "test@test.pl" && password === "1qaz@WSX"){
      return of({ token: "can_be_jwt_token" });
    }
    return throwError("Invalid credentials for user not exists")
  }
}
