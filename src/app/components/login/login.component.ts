import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

interface ILoginData {
  login: string,
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  data: any = {
    login: null,
    password: null
  };

  message: string = '';

  constructor(private router: Router, private authService: AuthenticationService, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
  }

  setDemoCredentials(): void {
    this.data.login = 'test@test.pl';
    this.data.password = '1qaz@WSX';
  }

  onFormSubmit(): void {
    const { login, password } = this.data;

    this.authService.login(login, password).subscribe({
      next: result => { 
        this.tokenStorageService.saveToken(result.token);
        this.router.navigate(["/"]) 
      },
      error: error => { 
        this.message = error;
        console.error(error)}
    });
  }
}
